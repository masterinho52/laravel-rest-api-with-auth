<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(
    App\Item::class,
    function (Faker $faker) {
        return [
            'code' => date('mdhis') . $faker->numberBetween(10, 99),
            'item_name' => $faker->word,
            'description' => $faker->word,
            'cost_price' => $faker->randomFloat(2, 20, 200),
            'selling_price' => $faker->randomFloat(2, 50, 500),
            'quantity' => $faker->randomDigit(),
            'id_category' => $faker->numberBetween(1, 5),
        ];
    }
);
