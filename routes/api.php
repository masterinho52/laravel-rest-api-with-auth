<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::get('holamundo', 'Api\UsersControler@holaMundo');


// Route::post('getitems', 'Api\itemsController@index');
// Route::post('showitem', 'Api\itemsController@show');
// Route::post('updateitem', 'Api\itemsController@update');
// Route::post('deleteitem', 'Api\itemsController@destroy');

Route::group(['middleware' => 'auth:api'], function () {
    /* test */
    Route::post('items', 'Api\ItemsController@store');
    Route::get('items', 'Api\ItemsController@index');
    Route::get('items/{id}', 'Api\ItemsController@show');

    Route::put('items/{id}', 'Api\ItemsController@update');
    Route::delete('items/{id}', 'Api\ItemsController@destroy');

    // Route::post('items', 'Api\ItemsController@store');
    // Route::resource('items', 'Api\ItemsController');


    
    /* users */
    Route::post('profile', 'Api\UsersControler@profile');
    Route::post('getusers', 'Api\UsersControler@getusers');
    

    /* items */
    // Route::post('getitems', 'Api\itemsController@index');
    // Route::post('showitem', 'Api\itemsController@show');
    // Route::post('updateitem', 'Api\itemsController@update');
    // Route::post('deleteitem', 'Api\itemsController@destroy');
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
