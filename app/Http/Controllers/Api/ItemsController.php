<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::join('categories', 'items.id_category', '=', 'categories.id')
            ->select(['items.id', 'items.item_name', 'items.description', 'items.selling_price', 'items.cost_price', 'items.quantity', 'categories.name as category', 'categories.id as cat_id'])
            ->get();
        return response()->json(compact('item'), 200);

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $items = new Item;
        if ((strlen(trim($request->code))) == 0) {
            $items->code = date('ymdHis');
        } else {
            $items->code = $request->code;
        }

        $items->item_name = $request->item_name;
        $items->cost_price = $request->cost_price;
        $items->description = $request->description;
        $items->selling_price = $request->selling_price;
        $items->quantity = $request->quantity;
        $items->id_category = $request->id_category;
        $items->save();
        $result = ['Guardado' => 'Exitoso'];

        return response()->json(compact('result'), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::find($id);
				// return $items;
        return response()->json(compact('item'), 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = Item::find($id);
            $user->update($request->all());


            $result = ['Actualizacion' => 'Exitosa'];

        } catch (\Illuminate\Database\QueryException $e) {
            $result = ['Actualizacion' => 'Fallida:' . $e];
        }

        return response()->json(compact('result'), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $items = Item::destroy($id);

            $result = ['Borrado' => 'Exitoso'];

        } catch (\Illuminate\Database\QueryException $e) {
            $result = ['Borrado' => 'Fallido:' . $e];
        }
        return response()->json(compact('result'), 200);

    }
}
