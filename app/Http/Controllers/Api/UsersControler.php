<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

use Illuminate\Support\Facades\Auth;
use Validator;

class UsersControler extends Controller
{

    public function profile()
    {
        $user = Auth::user();
        return response()->json(compact('user'), 200);
    }


    public function getusers()
    {
        $user = User::all();
        return response()->json(compact('user'), 200);
    }

    public function holaMundo()
    {
        $user = ['hola' => 'mundo'];
        return response()->json(compact('user'), 200);
    }
}
