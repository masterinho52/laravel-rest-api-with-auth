<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Julio Ramirez',
            'email' => 'jramirez@cacao.gt',
            'password' => bcrypt('jramirez')
        ]);

        factory(App\User::class, 2)->create();

    }
}
