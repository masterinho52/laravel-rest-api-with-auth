<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'item_name', 'description', 'selling_price', 'cost_price', 'quantity', 'id_categorie'
    ];

    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'created_at',
    ];
}
